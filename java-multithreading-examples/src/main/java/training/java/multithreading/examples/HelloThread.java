package training.java.multithreading.examples;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HelloThread extends Thread {

  public HelloThread(String name) {
    super(name);
  }

  @Override
  public void run() {
    log.info("Hello from a thread!");
  }

  public static void main(String args[]) {
    (new HelloThread("Hello-thread")).start();
  }
}
