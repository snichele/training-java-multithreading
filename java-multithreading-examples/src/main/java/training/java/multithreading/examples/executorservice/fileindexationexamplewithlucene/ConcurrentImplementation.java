package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.files.ConcurrentFileCrawler;
import training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.indexation.ConcurrentIndexer;

@Slf4j
public class ConcurrentImplementation {

  BlockingQueue<File> queue = new LinkedBlockingQueue<File>();
  @Getter
  AtomicInteger documentsIndexed = new AtomicInteger(0);
  AtomicInteger crawlersStartedCount;

  public static void main(String args[]) {
    ConcurrentImplementation implem = new ConcurrentImplementation();
    implem.runIndexation(2, IndexationConstants.DEFAULT_FILESYSTEM_ABSOLUTE_PATHS_TO_SCAN);
  }

  void runIndexation(int concurrentIndexerCount, String[] independantPathsToScan) {
    log.info("Running concurrent indexation ({} indexer concurrent tasks, {} paths to scan)", concurrentIndexerCount, independantPathsToScan);

    ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
    CountDownLatch firstIndexerStartingCountdownLatch = new CountDownLatch(1);
    crawlersStartedCount = new AtomicInteger(independantPathsToScan.length);
    for (String string : independantPathsToScan) {
      newFixedThreadPool.submit(new ConcurrentFileCrawler(queue, IndexationConstants.FILTER, new File(string), crawlersStartedCount));
    }

    for (int i = 0; i < concurrentIndexerCount; i++) {
      newFixedThreadPool.submit(new ConcurrentIndexer(queue, documentsIndexed, crawlersStartedCount,firstIndexerStartingCountdownLatch));
    }

    newFixedThreadPool.shutdown();
    try {
      newFixedThreadPool.awaitTermination(2, TimeUnit.MINUTES);
    } catch (InterruptedException ex) {
      log.error("Error, main launcher thread interrupted !");
    }
    log.info("Concurrent indexation terminated, indexed {} documents.", documentsIndexed.get());
  }
}
