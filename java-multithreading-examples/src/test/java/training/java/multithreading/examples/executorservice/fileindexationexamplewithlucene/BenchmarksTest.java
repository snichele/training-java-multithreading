package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene;

import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import com.carrotsearch.junitbenchmarks.annotation.AxisRange;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkMethodChart;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

// Warning, those 'tests' can pass only on the original developer machine, you should adapt it
// on your machine.
// We didn't wanted to provide an embedded packaged file structure to serve as fixture
// that would be too fast to crawl, or too complicated to maitain
@AxisRange(min = 0, max = 1)
@BenchmarkMethodChart(filePrefix = "benchmark-lists")
public class BenchmarksTest {

  @Rule
  public TestRule benchmarkRun = new BenchmarkRule();

  @BeforeClass
  public static void init() {
    System.setProperty("jub.consumers", "CONSOLE,H2");
    System.setProperty("jub.db.file", ".benchmarks");
  }

  @Test
  @BenchmarkOptions(benchmarkRounds = 5, warmupRounds = 2)
  public void run_concurrent_indexation_1crawler_1indexer() {
    assertCorrectIndexedDocumentsCountWithXIndexers(18, 1);
  }

  @Test
  @BenchmarkOptions(benchmarkRounds = 5, warmupRounds = 2)
  public void run_concurrent_indexation_1crawler_2indexers() {
    assertCorrectIndexedDocumentsCountWithXIndexers(18, 2);
  }

  @Test
  @BenchmarkOptions(benchmarkRounds = 5, warmupRounds = 2)
  public void run_concurrent_indexation_1crawler_3indexers() {
    assertCorrectIndexedDocumentsCountWithXIndexers(18, 3);
  }

  @Test
  @BenchmarkOptions(benchmarkRounds = 5, warmupRounds = 2)
  public void run_monothreaded() {
    MonothreadedImplementation monothreaded = new MonothreadedImplementation();
    monothreaded.runIndexation(new String[]{"C:\\Windows\\System32"});
    assertEquals(18, monothreaded.getDocumentsIndexed().get());
  }

  private void assertCorrectIndexedDocumentsCountWithXIndexers(int totalIndexedDocuments, int concurrentIndexersCount) {
    ConcurrentImplementation concurrent = new ConcurrentImplementation();
    concurrent.runIndexation(concurrentIndexersCount, new String[]{"C:\\Windows\\System32"});
    assertEquals(totalIndexedDocuments, concurrent.getDocumentsIndexed().get());
  }
}
