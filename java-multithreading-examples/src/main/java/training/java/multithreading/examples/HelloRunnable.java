package training.java.multithreading.examples;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HelloRunnable implements Runnable {

  public void run() {
    log.info("Hello from a thread!");
  }

  public static void main(String args[]) {
    (new Thread(new HelloRunnable())).start();
  }
}
