package training.java.multithreading.examples.javautilconcurrent.locking;

public class SynchronizedVersusLockExample {

  public static void main(String[] args) {
  }
}

/*
 * This class use the basic 'synchronized' keyword to ensure
 * synchronisation on a block of code.
 * Only one thread at a time can access this code block.
 * The accessing thread acquire the current object instance reentrant lock
 * and release it at the end of the block.
 * 
 * This is a 'low level' approach with a technically low semantic.
 * Thus, there is several problems :
 * - a synchronized block makes no guarantees about the sequence in which threads waiting to entering it are granted access.
 * - You cannot pass any parameters to the entry of a synchronized block. 
 *    Thus, having a timeout trying to get access to a synchronized block is not possible.
 * - The synchronized block must be fully contained within a single method. 
 *  A Lock can have it's calls to lock() and unlock() in separate methods.
 * 
 * We need a better way to perform this.
 */
class SynchronizedCounter {

  private int count = 0;

  public int inc() {
    synchronized (this) {
      return ++count;
    }
  }
}


/*
 * This class use a semantically better way to 'lock' and 'unlock'
 * parts of the code by using a Lock object. We are delegating the
 * 'low level' synchronisation to this object to preserve a good code 
 * semantic.
 *
 */
class LockedCustomCounter {

  private Lock lock = new Lock();
  private int count = 0;

  public int inc() throws InterruptedException {
    int newCount;
    lock.lock();
    try{
      // really simple code for example's sake
      newCount = ++count;
    } finally{
      lock.unlock();
    }
    return newCount;
  }
}

/*
 * Our own lock class.
 */
class Lock {

  boolean isLocked = false;
  Thread lockedBy = null;
  int lockedCount = 0;

  public synchronized void lock()
          throws InterruptedException {
    Thread callingThread = Thread.currentThread();
    while (isLocked && lockedBy != callingThread) {
      wait();
    }
    isLocked = true;
    lockedCount++;
    lockedBy = callingThread;
  }

  public synchronized void unlock() {
    if (Thread.currentThread() == this.lockedBy) {
      lockedCount--;

      if (lockedCount == 0) {
        isLocked = false;
        notify();
      }
    }
  }
}

/*
 * This class use a java util concurrent api and implementation of the Lock concept.
 * No need to redefine our own Lock !
 */
class LockedCounter {

  private java.util.concurrent.locks.Lock lock = new java.util.concurrent.locks.ReentrantLock();
  private int count = 0;

  public int inc() throws InterruptedException {
    int newCount;
    lock.lock();
    try{
      newCount = ++count;
    } finally{
      lock.unlock();
    }
    return newCount;
  }
}