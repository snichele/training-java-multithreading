package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.files;

import java.io.File;
import java.io.FileFilter;
import java.util.Queue;
import lombok.extern.slf4j.Slf4j;
import training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.Loggers;

@Slf4j
public class MonothreadedFileCrawler extends AbstractFileCrawler {

  public MonothreadedFileCrawler(Queue<File> fileQueue, FileFilter fileFilter, File root) {
    super(fileQueue, fileFilter, root);
  }

  @Override
  protected void doCrawling() {

    log.info("Started crawling at " + root);
    Loggers.TREATED_DOCUMENTS_LOGGER.info("Crawler runnable started");
    // recursive crawl from root
    crawl(root);
    log.info("Crawler runnable has finished.");
    Loggers.TREATED_DOCUMENTS_LOGGER.info("Crawler runnable has finished. Crawled {} files", totalFileCrawled);
  }

  private void crawl(File root) {
    File[] entries = root.listFiles(fileFilter);
    if (entries != null) {
      for (File entry : entries) {
        if (entry.isDirectory()) {
          crawl(entry);
        } else {
          fileQueue.add(entry);
          totalFileCrawled++;
          log.info("Added for indexation " + entry);
          Loggers.TREATED_DOCUMENTS_LOGGER.info("+{}", entry.getName());
        }
      }
    }
  }
}