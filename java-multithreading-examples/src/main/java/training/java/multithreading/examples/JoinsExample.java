package training.java.multithreading.examples;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JoinsExample {

  public static void main(String[] args) {

    final List<Long> valuesFetched = new CopyOnWriteArrayList<Long>();

    final Thread computerThread1 = new Thread("computerThread1") {
      @Override
      public void run() {
        log.info("I'm computing something from somewhere else...");
        try {
          Thread.sleep((long) (Math.random() * 10000L));
          valuesFetched.add(Long.valueOf(4807L));
        } catch (InterruptedException ex) {
          log.error("Thread have been interrupted !");
        }
      }
    };

    final Thread computerThread2 = new Thread("computerThread2") {
      @Override
      public void run() {
        log.info("I'm computing something from somewhere else too !...");
        try {
          Thread.sleep((long) (Math.random() * 10000L));
          valuesFetched.add(Long.valueOf(193L));
        } catch (InterruptedException ex) {
          log.error("Thread have been interrupted !");
        }

      }
    };

    Thread summerThread = new Thread("summerThread") {
      @Override
      public void run() {
        try {
          log.info("I'm waiting for other threads to finish !");
          computerThread1.join();
          computerThread2.join();
          Long total = Long.valueOf(0);
          for (Long nextValue : valuesFetched) {
            total = total + nextValue;
          }
          log.info("Other threads have finised and the result is {}", total);
        } catch (InterruptedException ex) {
          log.error("Thread have been interrupted !");
        }
      }
    };
    summerThread.start();
    computerThread1.start();
    computerThread2.start();
  }
}
