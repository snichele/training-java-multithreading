package training.java.multithreading.examples;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InterruptionExample {

  public static void main(String[] args) throws InterruptedException {
    Thread oneSleepyThread = new Thread() {
      @Override
      public void run() {
        try {
          log.info("I'm sleeping for 10 seconds...");
          Thread.sleep(10000);
          log.info("I have finished sleeping !");
        } catch (InterruptedException ex) {
          log.warn("Thread have been interrupted !");
        }
      }
    };
    oneSleepyThread.start();
    Thread.sleep(2000);
    oneSleepyThread.interrupt();

    Thread oneNiceThread = new Thread() {
      @Override
      public void run() {
        log.info("I'm starting to make some heavy calculations by batch...");
        while (!Thread.interrupted()) {
          log.info("I have not been interrupted, I'm treating the next batch !");
          treatNextBatch();
        }
        log.info("I have been interrupted, I'm stopping...");
      }

      private void treatNextBatch() {
        // Just some loops to eat cpu cycles.
        for (int i = 0; i < 10000; i++) {
          for (int j = 0; j < 10000; j++) {
            int k = j + i;
          }
        }
      }
    };
    oneNiceThread.start();
    Thread.sleep(1000); // give a chance to start at least one batch calculation...
    oneNiceThread.interrupt();
  }
}
