package training.java.multithreading.examples.producerconsummer;

import java.util.Random;
import lombok.extern.slf4j.Slf4j;

public class ProducerConsummerExample {

  public static final String TERMINATION_MESSAGE = "DONE";

  public static void main(String[] args) {
    Drop drop = new Drop();
    (new Thread(new Producer(drop),"Producer-thread")).start();
    (new Thread(new Consumer(drop),"Consumer-thread")).start();
  }
}

@Slf4j
class Drop {
  // Message sent from producer
  // to consumer.

  private String message;
  // True if consumer should wait
  // for producer to send message,
  // false if producer should wait for
  // consumer to retrieve message.
  private boolean empty = true;

  public synchronized String take() {
    // Wait until message is
    // available.
    while (empty) {
      log.debug("take () - empty !");
      try {
        log.debug("Caller thread should wait()");
        wait();
      } catch (InterruptedException e) {
      }
    }
    log.debug("take () - not empty !");

    // Toggle status.
    empty = true;
    log.debug("Mark as empty now.");
    // Notify producer that
    // status has changed.
    log.debug("'Wake up' all waiting threads");
    notifyAll();
    log.debug("Return message.");
    return message;
  }

  public synchronized void put(String message) {
    // Wait until message has
    // been retrieved.
    while (!empty) {
      log.debug("put () - not empty !");
      try {
        log.debug("Caller thread should wait()");
        wait();
      } catch (InterruptedException e) {
      }
    }
    log.debug("put () - empty !");
    // Toggle status.
    empty = false;
    log.debug("Mark as not empty now.");
    // Store message.
    this.message = message;
    // Notify consumer that status
    // has changed.
    log.debug("'Wake up' all waiting threads");
    notifyAll();
  }
}

@Slf4j
class Producer implements Runnable {

  private Drop drop;

  public Producer(Drop drop) {
    this.drop = drop;
  }

  public void run() {
    log.debug("Producer start !");
    String importantInfo[] = {
      "First message",
      "Second message",
      "Third message",
      "Fourth message"
    };
    Random random = new Random();

    for (int i = 0;
            i < importantInfo.length;
            i++) {
      log.debug("Producer put next message.");
      drop.put(importantInfo[i]);
      try {
        log.debug("Let's wait for some random time...");
        Thread.sleep(random.nextInt(5000));
      } catch (InterruptedException e) {
      }
    }
    drop.put(ProducerConsummerExample.TERMINATION_MESSAGE);
  }
}
@Slf4j
class Consumer implements Runnable {

  private Drop drop;

  public Consumer(Drop drop) {
    this.drop = drop;
  }

  public void run() {
    log.debug("Consummer start !");
    Random random = new Random();
    for (String message = drop.take();
            !message.equals(ProducerConsummerExample.TERMINATION_MESSAGE);
            message = drop.take()) {
      log.debug("Consummer read message.");
      System.out.format("MESSAGE RECEIVED: %s%n", message);
      try {
        log.debug("Let's wait for some random time...");
        Thread.sleep(random.nextInt(5000));
      } catch (InterruptedException e) {
      }
    }
  }
}
