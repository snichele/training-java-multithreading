package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.indexation;

import java.io.File;
import java.util.Queue;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractIndexer implements Runnable {

  protected final Queue<File> queue;

  public AbstractIndexer(Queue<File> queue) {
    this.queue = queue;
  }

  public void run() {
    log.info("Start indexing...");
    doIndexation();
  }

  protected abstract void doIndexation();
}
