package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.files;

import java.io.File;
import java.io.FileFilter;
import java.util.Queue;

public abstract class AbstractFileCrawler implements Runnable {
  protected final FileFilter fileFilter;
  protected final Queue<File> fileQueue;
  protected final File root;
  protected int totalFileCrawled;

  public AbstractFileCrawler(Queue<File> fileQueue, FileFilter fileFilter, File root) {
    this.fileQueue = fileQueue;
    this.fileFilter = fileFilter;
    this.root = root;    
  }

  public void run() {
    doCrawling();
  }

  protected abstract void doCrawling();
  
}
