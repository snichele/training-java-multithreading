package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene;

import java.io.File;
import java.io.FileFilter;

public class IndexationConstants {

  public static final String[] DEFAULT_FILESYSTEM_ABSOLUTE_PATHS_TO_SCAN = {"C:\\Windows\\System32"};
  public static final String FILE_EXTENSION_TO_RETAIN = ".log";
  public static final int NO_INDEXER_RUNNING = 0;
  public static final int TIME_TO_WAIT_FOR_NEXT_QUEUED_FILE_SECONDS = 5;
  public static final FileFilter FILTER = new FileFilter() {
    public boolean accept(File file) {
      return file.isDirectory() || file.getName().endsWith(FILE_EXTENSION_TO_RETAIN);
    }
  };
}
