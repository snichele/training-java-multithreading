package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.indexation;

import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

@Slf4j
class LuceneSupport {

  StandardAnalyzer analyzer;
  Directory index;
  IndexWriterConfig config;
  IndexWriter w;

  public LuceneSupport() {
    this.index = new RAMDirectory();
    this.analyzer = new StandardAnalyzer(Version.LUCENE_43);
    this.config = new IndexWriterConfig(Version.LUCENE_43, analyzer);
  }

  void addDoc(String title, String content) throws IOException {
    log.debug("Indexing document {} of size {}", title, content.length());
    Document doc = new Document();
    doc.add(new TextField("title", title, Field.Store.YES));
    doc.add(new TextField("content", content, Field.Store.YES));
    w.addDocument(doc);
  }

  void initIndexWriter() {
    log.info("Initialising IndexWriter...");
    if (w != null) {
      throw new IllegalStateException("Should not init and index writer already initialized ! Check your threading logic !");
    }
    try {
      w = new IndexWriter(index, config);
      log.info("Initialising IndexWriter Done !");
    } catch (IOException ex) {
      log.error("Error at initIndexWriter call()", ex);
      w = null;
    }
  }

  void closeIndexWriter() throws IOException {
    w.close();
    w = null;
  }
}
