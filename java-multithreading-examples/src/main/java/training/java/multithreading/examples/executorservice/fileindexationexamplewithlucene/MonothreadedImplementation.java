package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene;

import java.io.File;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.files.MonothreadedFileCrawler;
import training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.indexation.MonothreadedIndexer;

@Slf4j
public class MonothreadedImplementation {

  Queue<File> queue = new ArrayDeque<File>();
  @Getter
  AtomicInteger documentsIndexed = new AtomicInteger();

  public static void main(String args[]) {
    MonothreadedImplementation implem = new MonothreadedImplementation();
    implem.runIndexation(IndexationConstants.DEFAULT_FILESYSTEM_ABSOLUTE_PATHS_TO_SCAN);
  }

  void runIndexation(String[] independantPathsToScan) {
    log.info("Running monothreaded indexation ({} paths to scan)", new Object[]{independantPathsToScan});

    for (String string : independantPathsToScan) {
      new MonothreadedFileCrawler(queue, IndexationConstants.FILTER, new File(string)).run();
    }

    new MonothreadedIndexer(queue, documentsIndexed).run();

    log.info("Monothreaded indexation terminated, indexed {} documents.", documentsIndexed.get());
  }
}
