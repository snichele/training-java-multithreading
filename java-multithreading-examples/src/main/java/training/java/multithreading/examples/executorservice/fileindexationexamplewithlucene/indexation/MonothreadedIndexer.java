package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.indexation;

import java.io.File;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

@Slf4j
public class MonothreadedIndexer extends AbstractIndexer {
  
  private final static LuceneSupport LUCENE_SUPPORT = new LuceneSupport();
  private AtomicInteger documentsIndexed;
  
  public MonothreadedIndexer(Queue<File> queue, AtomicInteger documentsIndexed) {
    super(queue);
    this.documentsIndexed = documentsIndexed;
  }
  
  protected void doIndexation() {
    LUCENE_SUPPORT.initIndexWriter();
    for (File nextFileToIndex : queue) {
      try {
        indexFile(nextFileToIndex);
      } catch (IOException ex) {
        log.error("Error indexing file {}", nextFileToIndex, ex);
      }
    }
    try {
      LUCENE_SUPPORT.closeIndexWriter();
    } catch (IOException ex) {
      log.error("Error closing index ", ex);
    }
  }
  
  private void indexFile(File file) throws IOException {
    log.info("Indexing {}", file);
    LUCENE_SUPPORT.addDoc(file.getName(), FileUtils.readFileToString(file));
    this.documentsIndexed.incrementAndGet();
    log.debug("Done indexing {}", file);
  }
}