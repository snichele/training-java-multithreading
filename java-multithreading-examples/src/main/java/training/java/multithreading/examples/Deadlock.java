package training.java.multithreading.examples;

/**
 * This will not deadlock evety time. Try to launch the program several times.
 * 
 */
public class Deadlock {

  static class Friend {

    private final String name;

    public Friend(String name) {
      this.name = name;
    }

    public String getName() {
      return this.name;
    }

    public synchronized void bow(Friend bower) {
      System.out.format("%s: %s"
              + "  has bowed to me!%n",
              this.name, bower.getName());
      bower.bowBack(this);
    }

    public synchronized void bowBack(Friend bower) {
      System.out.format("%s: %s"
              + " has bowed back to me!%n",
              this.name, bower.getName());
    }
  }

  public static void main(String[] args) {
    final Friend john =
            new Friend("John");
    final Friend bob =
            new Friend("Bob");
    new Thread(new Runnable() {
      public void run() {
        john.bow(bob);
      }
    }).start();
    new Thread(new Runnable() {
      public void run() {
        bob.bow(john);
      }
    }).start();
  }
}