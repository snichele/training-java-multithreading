package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.indexation;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.IndexationConstants;
import training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.Loggers;

@Slf4j
public class ConcurrentIndexer extends AbstractIndexer {

  private final static LuceneSupport LUCENE_SUPPORT = new LuceneSupport();
  private static AtomicInteger numberOfIndexerStarted = new AtomicInteger(0);
  private boolean hasMoreFileToIndex = true;
  private CountDownLatch countdownLatch;
  private AtomicInteger documentsIndexedByAllIndexersCount;
  private AtomicInteger crawlersStartedCount;
  private int documentIndexedByMeCount;

  public ConcurrentIndexer(BlockingQueue<File> queue, AtomicInteger documentsIndexed, AtomicInteger crawlersStartedCount, CountDownLatch countdownLatch) {
    super(queue);
    this.documentsIndexedByAllIndexersCount = documentsIndexed;
    this.crawlersStartedCount = crawlersStartedCount;
    this.countdownLatch = countdownLatch;
  }

  protected void doIndexation() {
    if (!(this.queue instanceof BlockingQueue)) {
      throw new IllegalArgumentException("Concurrent implementation needs a BlockingQueue instance to works !");
    }
    Loggers.TREATED_DOCUMENTS_LOGGER.info("ConcurrentIndexer runnable doIndexation() started (crawlersStartedCount = {})", crawlersStartedCount.get());
    BlockingQueue<File> blockingQueue = (BlockingQueue<File>) this.queue;
    int indexersAlreadyStartedCount = numberOfIndexerStarted.getAndIncrement();
    if (indexersAlreadyStartedCount == IndexationConstants.NO_INDEXER_RUNNING) {
      log.info("First indexer started. Initialising index.");
      Loggers.TREATED_DOCUMENTS_LOGGER.info("First indexer started. Initialising index.");
      LUCENE_SUPPORT.initIndexWriter();
      countdownLatch.countDown();
    } else {
      try {
        Loggers.TREATED_DOCUMENTS_LOGGER.info("Another ConcurrentIndexer runnable is initialising index, I must wait...");
        countdownLatch.await();
        Loggers.TREATED_DOCUMENTS_LOGGER.info("Ok I can go !");
      } catch (InterruptedException ex) {
        log.error("Thread interrupted while waiting for latch to be released !", ex);
      }
    }
    while ((crawlersStartedCount.get() > 0) || hasMoreFileToIndex) {
      log.debug(" Try to consume (poll) from queue ( which contains {} elements)", queue.size());
      
      File nextFileToIndex = null;
      try {
        nextFileToIndex = blockingQueue.poll(10, TimeUnit.MILLISECONDS);
      } catch (InterruptedException ex) {
        log.error("Interrupted while waiting for polling !");
      }
      if (nextFileToIndex != null) {
        Loggers.TREATED_DOCUMENTS_LOGGER.info("-{}", nextFileToIndex.getName());
        try {
          indexFile(nextFileToIndex);
          Loggers.TREATED_DOCUMENTS_LOGGER.info("After indexing {} || {}",crawlersStartedCount.get(),hasMoreFileToIndex);
        } catch (Exception ex) {
          log.error("Error indexing file", ex);
        }
      } else {
        if((crawlersStartedCount.get() == 0)){
          hasMoreFileToIndex = false;
        }
      }
    }

    int indexersStillStarted = numberOfIndexerStarted.decrementAndGet();
    if (indexersStillStarted == IndexationConstants.NO_INDEXER_RUNNING) {
      log.info("Last indexer finished. Closing index.");
      Loggers.TREATED_DOCUMENTS_LOGGER.info("Last indexer finished. Closing index.");
      try {
        LUCENE_SUPPORT.closeIndexWriter();
      } catch (IOException ex) {
        log.error("Error closing index ", ex);
        Loggers.TREATED_DOCUMENTS_LOGGER.error("Error closing index ", ex);
      }
    }
    Loggers.TREATED_DOCUMENTS_LOGGER.info("Indexer finished (indexed {} on a total of {}).",documentIndexedByMeCount,documentsIndexedByAllIndexersCount.get());
  }

  private void indexFile(File file) throws InterruptedException, IOException {
    log.info("Indexing {}", file);
    LUCENE_SUPPORT.addDoc(file.getName(), FileUtils.readFileToString(file));
    this.documentsIndexedByAllIndexersCount.incrementAndGet();
    log.debug("Done indexing {}", file);
    Loggers.TREATED_DOCUMENTS_LOGGER.info("*{}", file.getName());
    documentIndexedByMeCount++;
  }
}