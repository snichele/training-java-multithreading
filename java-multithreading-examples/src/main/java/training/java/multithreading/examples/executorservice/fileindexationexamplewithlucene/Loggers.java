package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Loggers {
  public static final Logger TREATED_DOCUMENTS_LOGGER = LoggerFactory.getLogger("TREATED_DOCUMENTS_LOGGER");
}
