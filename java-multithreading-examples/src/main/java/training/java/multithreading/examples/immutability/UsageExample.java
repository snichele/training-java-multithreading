package training.java.multithreading.examples.immutability;

import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.extern.slf4j.Slf4j;

public class UsageExample {

  public static void main(String[] args) {
    SynchronizedRGBColor color =
            new SynchronizedRGBColor(0, 0, 0, "Pitch Black");

    Thread t1 = new Thread(new RGBColorWrangler(color), "t1");
    Thread t2 = new Thread(new RGBColorWrangler(color), "t2");
    t1.start();
    t2.start();
  }
}

@Slf4j
class RGBColorWrangler implements Runnable {

  private SynchronizedRGBColor color;

  public RGBColorWrangler(SynchronizedRGBColor color) {
    this.color = color;
  }

  public void run() {
    readAndModifyColor();
    try {
      Thread.sleep((long) (Math.random()*2000));
      readAndModifyColor();
      Thread.sleep((long) (Math.random()*500));
      readAndModifyColor();
    } catch (InterruptedException ex) {
    }
  }

  private int random0to255Int() {
    return (int) (Math.random() * 255d);
  }

  private void readAndModifyColor() {
    log.debug("Color rgb is : {}", color.getRGB());
    log.debug("Color name is : {}", color.getName());
    log.debug("Now I change it !");
    int r = random0to255Int();
    int g = random0to255Int();
    int b = random0to255Int();
    color.set(r, g, b, "" + r + "-" + g + "-" + b);
  }
}
