package training.java.multithreading.examples;

import lombok.extern.slf4j.Slf4j;

/**
 * To remember about synchronisation
 * - a thread calling a synchronized method acquire a hold on a 'lock'
 *  - 'lock' = this, for an object instance
 *  - 'lock' = the 'class' object for a static method
 * - never call other objects methods from a synchronized method (liveness errors !)
 * 
 * - you can synchronize any code block
 *  - usefull for fine grained synchronisation
 */
public class SynchronizedMethod {
  
  public static void main(String[] args) {
    SynchronizedCounter sc = new SynchronizedCounter();
    Thread t = new Thread(new CounterAccessor(sc), "t1");
    Thread t2 = new Thread(new CounterAccessor(sc), "t2");
    Thread t3 = new Thread(new CounterAccessor(sc), "t3");
    t.start();
    t2.start();
    t3.start();
    
  }
}

@Slf4j
class SynchronizedCounter {
  
  private int c = 0;
  
  public synchronized void increment() {
    log.debug("Incrementing");
    long sleepValue = (long) (Math.random() * 2000);
    log.debug("Thread sleep to illustrate synchronization...({} ms )", sleepValue);
    try {
      Thread.sleep(sleepValue);
    } catch (InterruptedException ex) {
      log.error("Interrupted !");
    }
    c++;
    log.debug("Finished incrementing, i leave !");
  }
  
  public synchronized void decrement() {
    log.debug("Decrementing");
    c--;
  }
  
  public synchronized int value() {
    log.debug("Getting value");
    return c;
  }
}

@Slf4j
class CounterAccessor implements Runnable {
  
  private SynchronizedCounter sc;
  
  public CounterAccessor(SynchronizedCounter sc) {
    this.sc = sc;
  }
  
  public void run() {
    log.info("I increment");
    sc.increment();
    log.info("I decrement");
    sc.decrement();
    log.info("I read : {}", sc.value());
  }
}
