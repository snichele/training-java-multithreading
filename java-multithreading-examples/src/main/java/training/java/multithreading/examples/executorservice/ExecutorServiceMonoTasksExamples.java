package training.java.multithreading.examples.executorservice;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExecutorServiceMonoTasksExamples {

  public static void main(String[] args) throws InterruptedException, ExecutionException {
    // Enable one of the following lines at a time.
//    NonBloquingExecutor.run();
//    BloquingExecutor.run();
//    ExecutorWithRunnableAndFuture.run();
    ExecutorWithCallableAndFuture.run();
  }
}

@Slf4j
class NonBloquingExecutor {

  public static void run() throws InterruptedException {
    log.info("Start");
    ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
    // Submit a new task for execution
    log.info("Submit a new task !");
    exec.execute(new Runnable() {
      public void run() {
        try {
          Thread.sleep(2000);
          log.info("I will execute myself at some time in the future and cannot return nothing.");
        } catch (InterruptedException ex) {
          log.error("Interruped", ex);
        }
      }
    });
    // Do not accept anymore tasks.
    // Pool will be destroyed after tasks finish, but let the current thread continue
    exec.shutdown();
    log.info("Main thread terminated, pool needs to finish working.");

  }
}

@Slf4j
class BloquingExecutor {

  public static void run() throws InterruptedException {
    log.info("Start");
    ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
    // Submit a new task for execution
    log.info("Submit a new task !");
    exec.execute(new Runnable() {
      public void run() {
        try {
          Thread.sleep(2000);
          log.info("I will execute myself at some time in the future and cannot return nothing.");
        } catch (InterruptedException ex) {
          log.error("Interruped", ex);
        }
      }
    });
    // Do not accept anymore tasks.
    // Pool will be destroyed after tasks finish, but let the current thread continue
    exec.shutdown();
    // Now we precise that we want to wait for termination and block current Thread
    exec.awaitTermination(1000, TimeUnit.SECONDS);
    log.info("Main thread terminated, pool is supposed to have finished working.");
  }
}

@Slf4j
class ExecutorWithRunnableAndFuture {

  public static void run() throws InterruptedException, ExecutionException {
    log.info("Start");
    ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
    // Submit a new task for execution
    log.info("Submit a new task !");
    Future<?> future = exec.submit(new Runnable() {
      public void run() {
        try {
          Thread.sleep(4000);
          log.info("I will execute myself at some time in the future and cannot return nothing.");
        } catch (InterruptedException ex) {
          log.error("Interruped", ex);
        }
      }
    });
    // Do not accept anymore tasks.
    // Pool will be destroyed after tasks finish, but let the current thread continue
    exec.shutdown();
    log.info("Now calling future.get() that is supposed to block the current thread execution.");
    future.get();
    log.info("Main thread terminated, future has finally answered.");
  }
}

@Slf4j
class ExecutorWithCallableAndFuture {

  public static void run() throws InterruptedException, ExecutionException {
    log.info("Start");
    ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
    // Submit a new task for execution
    log.info("Submit a new task !");
    Future<String> future = exec.submit(new Callable<String>() {
      public String call() throws Exception {
        try {
          Thread.sleep(4000);
          log.info("I will execute myself at some time in the future and cannot return nothing.");
        } catch (InterruptedException ex) {
          log.error("Interruped", ex);
        }
        return "**Future response**";
      }
    });
    // Do not accept anymore tasks.
    // Pool will be destroyed after tasks finish, but let the current thread continue
    exec.shutdown();
    log.info("Now calling future.get() that is supposed to block the current thread execution.");
    String returned = future.get();
    log.info("Main thread terminated, future has finally answered with {}", returned);
  }
}
