package training.java.multithreading.examples;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SleepMessages {

  public static void main(String args[])
          throws InterruptedException {
    String importantInfo[] = {
      "First message",
      "Second message",
      "Third message",
      "Fourth message"
    };

    for (int i = 0; i < importantInfo.length; i++) {
      //Pause for 4 seconds
      Thread.sleep(4000);
      //Print a message
      log.info(importantInfo[i]);
    }
  }
}