package training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.files;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.extern.slf4j.Slf4j;
import training.java.multithreading.examples.executorservice.fileindexationexamplewithlucene.Loggers;

@Slf4j
public class ConcurrentFileCrawler extends AbstractFileCrawler {

  private final AtomicInteger totalFileCrawlerStarted;

  public ConcurrentFileCrawler(BlockingQueue<File> fileQueue, FileFilter fileFilter, File root, AtomicInteger totalFileCrawlerStarted) {
    super(fileQueue, fileFilter, root);
    this.totalFileCrawlerStarted = totalFileCrawlerStarted;
  }

  @Override
  protected void doCrawling() {
    try {
      log.info("Started crawling at " + root);
      Loggers.TREATED_DOCUMENTS_LOGGER.info("Crawler runnable started");
      // recursive crawl from root
      crawl(root);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
    log.info("Crawler runnable has finished.");
    Loggers.TREATED_DOCUMENTS_LOGGER.info("Crawler runnable has finished. Crawled {} files", totalFileCrawled);
    int decrementedValue = totalFileCrawlerStarted.decrementAndGet();
    if (decrementedValue < 0) {
      throw new IllegalStateException("FileCrawler count has decreased below zero, that's not normal !");
    }
  }

  private void crawl(File root) throws InterruptedException {
    File[] entries = root.listFiles(fileFilter);
    if (entries != null) {
      for (File entry : entries) {
        if (entry.isDirectory()) {
          crawl(entry);
        } else {
          fileQueue.add(entry);
          totalFileCrawled++;
          log.info("Added for indexation " + entry);
          Loggers.TREATED_DOCUMENTS_LOGGER.info("+{}", entry.getName());
        }
      }
    }
  }
}